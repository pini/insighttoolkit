Source: insighttoolkit4
Homepage: http://www.itk.org/
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steve M. Robbins <smr@debian.org>,
           Gert Wollny <gewo@debian.org>
Build-Depends: debhelper (>= 9),
    cmake (>= 2.8.9), 
    swig3.0,
    castxml (>= 0.1+git20160202-1.1),
    g++-8, 
    zlib1g-dev (>= 1.2.2), 
    libdouble-conversion-dev,
    libexpat-dev,
    libpng-dev,
    libtiff-dev,
    libfftw3-dev,
    libdcmtk-dev (>= 3.6.1~20150924-4), 
    libgdcm2-dev (>= 2.6.1-3) | libgdcm2-dev ( << 2.6.0 ),
    libgtest-dev,
    uuid-dev, 
    libminc-dev, 
    libnifti-dev, 
    libhdf5-dev, 
    python3-dev
#	libvtk6-dev -- only needed if we enable one of the following modules:
# VtkGlue, LevelSetsv4Visualization
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/med-team/insighttoolkit
Vcs-Git: https://salsa.debian.org/med-team/insighttoolkit.git

Package: libinsighttoolkit4.13
Section: libs
Architecture: amd64 i386
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Image processing toolkit for registration and segmentation - runtime
 ITK is an open-source software toolkit for performing registration and
 segmentation. Segmentation is the process of identifying and
 classifying data found in a digitally sampled
 representation. Typically the sampled representation is an image
 acquired from such medical instrumentation as CT or MRI
 scanners. Registration is the task of aligning or developing
 correspondences between data. For example, in the medical
 environment, a CT scan may be aligned with a MRI scan in order to
 combine the information contained in both.
 .
 This package contains the libraries needed to run ITK applications.

Package: libinsighttoolkit4-dev
Section: libdevel
Architecture: amd64 i386
Depends: ${shlibs:Depends}, ${misc:Depends}, libinsighttoolkit4.13 (= ${binary:Version}), libgdcm2-dev, libdcmtk-dev, libhdf5-dev, libdouble-conversion-dev, libexpat-dev, libnifti-dev
Recommends: libfftw3-dev, uuid-dev
Conflicts: libinsighttoolkit-dev, libinsighttoolkit3-dev
Replaces: libinsighttoolkit-dev
Suggests: insighttoolkit4-examples
Description: Image processing toolkit for registration and segmentation - development
 ITK is an open-source software toolkit for performing registration and
 segmentation. Segmentation is the process of identifying and
 classifying data found in a digitally sampled
 representation. Typically the sampled representation is an image
 acquired from such medical instrumentation as CT or MRI
 scanners. Registration is the task of aligning or developing
 correspondences between data. For example, in the medical
 environment, a CT scan may be aligned with a MRI scan in order to
 combine the information contained in both.
 .
 This package contains the development files needed to build your own
 ITK applications.

Package: insighttoolkit4-examples
Section: devel
Architecture: all
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: libinsighttoolkit4-dev
Conflicts: insighttoolkit-examples
Replaces: insighttoolkit-examples
Description: Image processing toolkit for registration and segmentation - examples
 ITK is an open-source software toolkit for performing registration and
 segmentation. Segmentation is the process of identifying and
 classifying data found in a digitally sampled
 representation. Typically the sampled representation is an image
 acquired from such medical instrumentation as CT or MRI
 scanners. Registration is the task of aligning or developing
 correspondences between data. For example, in the medical
 environment, a CT scan may be aligned with a MRI scan in order to
 combine the information contained in both.
 .
 This package contains the source for example programs.

Package: insighttoolkit4-python 
Section: python 
Architecture: amd64 i386
Depends: ${misc:Depends}, ${shlibs:Depends}, ${python3:Depends}
Conflicts: insighttoolkit-python
Replaces: insighttoolkit-python
Description: Image processing toolkit for registration and segmentation - Python bindings
 ITK is an open-source software toolkit for performing registration and
 segmentation. Segmentation is the process of identifying and
 classifying data found in a digitally sampled
 representation. Typically the sampled representation is an image
 acquired from such medical instrumentation as CT or MRI
 scanners. Registration is the task of aligning or developing
 correspondences between data. For example, in the medical
 environment, a CT scan may be aligned with a MRI scan in order to
 combine the information contained in both.
 .
 This package contains the Python bindings. 
